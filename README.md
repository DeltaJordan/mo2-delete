# Mod Organizer 2 Removal Tool

This is a tool for Mod Organizer 2 that simplifies removing files from mods. It supports relative paths with wildcards (*). See usage examples below.

This tool also supports Lexy's LotD Modding Guide removal instructions. Just copy and paste the lines that the guide asks for, and the tool will do the rest of the work.

## Installation
Download `mo2-delete.py` and put the file into your MO2 plugins folder. NOTE: If you were using any version of this tool before 2.0.0 you should delete the old version before installing the latest. The file names have changed. *Theoretically, this shouldn't break anything but it's best not to take the chance. Don't say I didn't warn you.*

## Setup
Within the plugin settings, you MUST change 'removal-type' to either 'mohidden' (adds "*.mohidden" to the end of the file/folder path) or 'delete' (deletes the files from your filesystem). Otherwise, the tool will NOT run. This is to ensure you do not do something to your files that you aren't expecting. Call it an idiot check ;)

## Usage
1. Select the "MO2 Removal Tool" option from the tools menu in the MO2 toolbar.
2. Type the name of the mod you want to remove files/folders from (The last mod in your modlist is selected by default).
3. Type/Paste the paths into the box below the mod selector.
4. Click Run. Another dialog will open.
5. Review the files that will be removed. Any wildcard paths will be expanded to show what files were selected.
6. Click "Remove All" to finalize the deletion. Note that you can also invert the selection to remove everything EXCEPT what you originally entered. THIS IS EXPERIMENTAL. Remember that you CANNOT undo a deletion from this tool, I personally recommend using the "mohidden" option when using this feature.
7. A file will be created in the root folder of the mod to log the removals you made. The file will be called {modName}.{'delete'|'mohidden'}.log

A visual representation of the above is shown below:
![usage gif](docs/usage.gif)

## Support
You can either create an issue here, or post a bug/comment to [the NexusMods page](https://www.nexusmods.com/skyrimspecialedition/mods/117306).
