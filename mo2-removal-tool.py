import glob
import os
import shutil
import time
import traceback

from typing import List
from pathlib import Path
from enum import Enum

from mobase import (
    IPlugin,
    IPluginTool,
    VersionInfo,
    PluginSetting,
    IOrganizer,
    ReleaseType,
    IModInterface,
)

from PyQt6.QtGui import QIcon, QColor
from PyQt6.QtWidgets import (
    QWidget,
    QSizePolicy,
    QMessageBox,
    QCompleter,
    QProgressDialog,
    QApplication,
)
import PyQt6.QtWidgets as QtWidgets
from PyQt6.QtCore import Qt, QSettings

qtBlack = Qt.GlobalColor.black
qtUserRole = Qt.ItemDataRole.UserRole
qtScrollBarAlwaysOff = Qt.ScrollBarPolicy.ScrollBarAlwaysOff
qtCustomContextMenu = Qt.ContextMenuPolicy.CustomContextMenu
qtWindowContextHelpButtonHint = Qt.WindowType.WindowContextHelpButtonHint

DELETE_OPTION = "delete"
HIDDEN_OPTION = "mohidden"

LOG_WIDTH = 100

PLUGIN_EXTS = [".esp", ".esm", ".esl"]


def isdirectsubpath(folder: str, file: str) -> bool:
    full_path = os.path.join(folder, os.path.basename(file))
    return os.path.samefile(full_path, file)


class RemovalEntryType(Enum):
    FILE = 1
    DIRECTORY = 2
    INVALID = 3


class RemovalEntry:
    def __init__(self, path: str, type: RemovalEntryType) -> None:
        self.path = path
        self.type = type


class OptionalEntry:
    def __init__(self, failed: bool, path: str) -> None:
        self.failed = failed
        self.path = path


class WalkFailedEntry:
    def __init__(self, path: str, stackTrace: str) -> None:
        self.stackTrace: str = stackTrace
        self.path: str = path


class WalkedHiddenResult:
    def __init__(self) -> None:
        self.walkFailed: bool = False
        """
        If the entire walk failed, this is true.
        Otherwise, failed files will be in the failed list.
        Currently unused, but useful so I'm keeping it.
        """
        self.succeeded: list[str] = []
        self.failed: list[WalkFailedEntry] = []


class PluginWindow(QtWidgets.QDialog):
    def __init__(self, organizer: IOrganizer):
        self.__organizer = organizer

        super(PluginWindow, self).__init__(None)

        removal_type = organizer.pluginSetting(PluginTool.NAME, "removal-type")

        if isinstance(removal_type, str):
            self.__removalType: str = removal_type
        else:
            warningMsg = QMessageBox()
            warningMsg.setIcon(QMessageBox.Icon.Critical)
            warningMsg.setText("Invalid removal-type option.")
            warningMsg.setInformativeText(
                'Your setting for "removal-type" is invalid. Go to the plugin settings and set it to \''
                + DELETE_OPTION
                + "' for deletion or '"
                + HIDDEN_OPTION
                + "' to hide the files by renaming the files/folders to *.mohidden"
            )
            warningMsg.setWindowTitle("Error")
            warningMsg.setStandardButtons(QMessageBox.StandardButton.Ok)
            warningMsg.exec()
            self.close()
            return

        if (
            self.__removalType.casefold() != HIDDEN_OPTION.casefold()
            and self.__removalType.casefold() != DELETE_OPTION.casefold()
        ):
            warningMsg = QMessageBox()
            warningMsg.setIcon(QMessageBox.Icon.Critical)
            warningMsg.setText("Invalid removal-type option.")
            warningMsg.setInformativeText(
                'Your setting for "removal-type" is invalid. Go to the plugin settings and set it to \''
                + DELETE_OPTION
                + "' for deletion or '"
                + HIDDEN_OPTION
                + "' to hide the files by renaming the files/folders to *.mohidden"
            )
            warningMsg.setWindowTitle("Error")
            warningMsg.setStandardButtons(QMessageBox.StandardButton.Ok)
            warningMsg.exec()
            self.close()
            return

        self.resize(500, 500)
        settings = QSettings(PluginTool.AUTHOR, PluginTool.NAME)
        savedGeometry = settings.value("geometry-main")
        if savedGeometry:
            self.restoreGeometry(settings.value("geometry-main"))
        self.setWindowFlags(self.windowFlags() & ~qtWindowContextHelpButtonHint)

        # Vertical Layout
        verticalLayout = QtWidgets.QVBoxLayout()

        # Vertical Layout -> DropDown Layout
        dropDownLayout = QtWidgets.QHBoxLayout()

        # Needs to be created early for signal connection.
        self.removeTextEdit = QtWidgets.QTextEdit()

        # Vertical Layout -> DropDown Layout -> Mod Select DropDown
        self.modSelect = QtWidgets.QComboBox()
        self.modSelect.setEditable(True)
        self.modSelect.currentIndexChanged.connect(self.removeTextEdit.clear)
        dropDownLayout.addWidget(self.modSelect)

        # Vertical Layout -> DropDown Layout -> Refresh Button
        refreshButton = QtWidgets.QPushButton("", self)
        refreshButton.setIcon(QIcon(":/MO/gui/refresh"))
        refreshButton.clicked.connect(self.refreshModList)
        refreshButton.setSizePolicy(QSizePolicy.Policy.Fixed, QSizePolicy.Policy.Fixed)
        dropDownLayout.addWidget(refreshButton)

        verticalLayout.addLayout(dropDownLayout)

        # Vertical Layout -> Remove TextEdit
        self.removeTextEdit.setLineWrapMode(QtWidgets.QTextEdit.LineWrapMode.NoWrap)
        self.removeTextEdit.setSizePolicy(
            QSizePolicy.Policy.Expanding, QSizePolicy.Policy.Expanding
        )
        self.removeTextEdit.setAcceptRichText(False)
        self.removeTextEdit.setPlaceholderText("Enter path(s) to delete here...")

        verticalLayout.addWidget(self.removeTextEdit)

        # Vertical Layout -> Button Layout
        buttonLayout = QtWidgets.QHBoxLayout()

        # Vertical Layout -> Button Layout -> Run Button
        runButton = QtWidgets.QPushButton("Run", self)
        runButton.setIcon(QIcon(":/MO/gui/check"))
        runButton.clicked.connect(self.runRemoval)
        buttonLayout.addWidget(runButton)

        # Vertical Layout -> Button Layout -> Close Button
        closeButton = QtWidgets.QPushButton("Close", self)
        closeButton.clicked.connect(self.closeWindow)
        buttonLayout.addWidget(closeButton)

        verticalLayout.addLayout(buttonLayout)

        self.setLayout(verticalLayout)
        self.refreshModList()

    def closeWindow(self) -> None:
        self.close()

    def closeEvent(self, event):
        settings = QSettings(PluginTool.AUTHOR, PluginTool.NAME)
        settings.setValue("geometry-main", self.saveGeometry())
        QtWidgets.QDialog.closeEvent(self, event)

    def refreshModList(self):
        self.modSelect.clear()
        modNameList = []
        lastMod = self.__organizer.modList().allModsByProfilePriority()[-1]
        index = 0
        lastModIndex = 0
        for modName in sorted(self.__organizer.modList().allMods()):
            if modName == lastMod:
                lastModIndex = index
            index += 1
            modNameList.append(modName)

        # Mod list autocomplete for combobox
        modListCompleter = QCompleter(modNameList, self)
        modListCompleter.setCaseSensitivity(Qt.CaseSensitivity.CaseInsensitive)

        self.modSelect.addItems(modNameList)
        self.modSelect.setCurrentIndex(lastModIndex)
        self.modSelect.setCompleter(modListCompleter)

    def formatRemoveTextEdit(self):
        text = self.removeTextEdit.toPlainText()
        newText = []
        for line in text.split("\n"):
            if not (not line or not line.strip()):
                newText.append(line.strip())
        self.removeTextEdit.setText("\n".join(newText))

    def runRemoval(self):
        self.formatRemoveTextEdit()  # Remove any padding in removeTextEdit
        modInternalName = self.modSelect.currentText()
        mod = self.__organizer.modList().getMod(modInternalName)
        resultInfo: list[RemovalEntry] = []
        if mod is not None:
            lines = self.removeTextEdit.toPlainText().split("\n")
            pathsToRemove: list[str] = []
            modDir = mod.absolutePath()
            for line in lines:
                pathsToRemove.append(os.path.join(glob.escape(modDir), line))
            for path in pathsToRemove:
                globs = glob.glob(path)
                if len(globs) == 0:
                    resultInfo.append(RemovalEntry(path, RemovalEntryType.INVALID))
                else:
                    for exactPath in globs:
                        if (
                            not Path(modDir)
                            in Path(os.path.realpath(exactPath)).parents
                        ):
                            continue
                        if any(
                            x.type != RemovalEntryType.INVALID
                            and os.path.samefile(exactPath, x.path)
                            for x in resultInfo
                        ):
                            # Skip duplicate entries to prevent errors during removal.
                            continue
                        elif os.path.isfile(exactPath) or os.path.islink(exactPath):
                            if (
                                self.__removalType.casefold()
                                == HIDDEN_OPTION.casefold()
                                and os.path.splitext(exactPath)[-1].lower()
                                == ".mohidden"
                            ):
                                continue
                            resultInfo.append(
                                RemovalEntry(exactPath, RemovalEntryType.FILE)
                            )
                        elif os.path.isdir(exactPath):
                            resultInfo.append(
                                RemovalEntry(exactPath, RemovalEntryType.DIRECTORY)
                            )
                        else:
                            resultInfo.append(
                                RemovalEntry(exactPath, RemovalEntryType.INVALID)
                            )
            confirmationWindow = ConfirmationWindow(
                mod, self, resultInfo, self.__organizer
            )
            confirmationWindow.open()
        else:
            warningMsg = QMessageBox()
            warningMsg.setIcon(QMessageBox.Icon.Critical)
            warningMsg.setText("Invalid mod entered.")
            warningMsg.setWindowTitle("Error")
            warningMsg.setStandardButtons(QMessageBox.StandardButton.Ok)
            warningMsg.exec()


class ConfirmationWindow(QtWidgets.QDialog):
    def __init__(
        self,
        mod: IModInterface,
        parent: QWidget,
        resultInfo: list[RemovalEntry],
        organizer: IOrganizer,
    ):
        self.__organizer = organizer

        removal_type = organizer.pluginSetting(PluginTool.NAME, "removal-type")
        if isinstance(removal_type, str):
            self.__removalType = removal_type
        else:
            warningMsg = QMessageBox()
            warningMsg.setIcon(QMessageBox.Icon.Critical)
            warningMsg.setText("Invalid removal-type option.")
            warningMsg.setInformativeText(
                'Your setting for "removal-type" is invalid. Go to the plugin settings and set it to \''
                + DELETE_OPTION
                + "' for deletion or '"
                + HIDDEN_OPTION
                + "' to hide the files by renaming the files/folders to *.mohidden"
            )
            warningMsg.setWindowTitle("Error")
            warningMsg.setStandardButtons(QMessageBox.StandardButton.Ok)
            warningMsg.exec()
            self.close()
            return

        self.__resultInfo = resultInfo
        self.__debugEnabled = organizer.pluginSetting(PluginTool.NAME, "debug-log")

        self.__pluginOptional = (
            organizer.pluginSetting(PluginTool.NAME, "plugin-optional") == True
        )

        settings = QSettings(PluginTool.AUTHOR, PluginTool.NAME)
        if self.__pluginOptional and not settings.value("optional-warning"):
            settings.setValue("optional-warning", True)
            warningMsg = QMessageBox()
            warningMsg.setIcon(QMessageBox.Icon.Warning)
            warningMsg.setText(
                "The optional setting cannot work if duplicates"
                + " exist in the optional folder of the mod."
                + " Always check this tools work, it's not always perfect!"
                + "\nThis message will not show again."
            )
            warningMsg.setWindowTitle("Warning")
            warningMsg.setStandardButtons(QMessageBox.StandardButton.Ok)
            warningMsg.exec()

        self.__mod = mod

        super(ConfirmationWindow, self).__init__(parent)

        self.resize(700, 500)
        settings = QSettings(PluginTool.AUTHOR, PluginTool.NAME)
        savedGeometry = settings.value("geometry-confirm")
        if savedGeometry:
            self.restoreGeometry(settings.value("geometry-confirm"))
        self.setWindowFlags(self.windowFlags() & ~qtWindowContextHelpButtonHint)

        # Vertical Layout
        verticalLayout = QtWidgets.QVBoxLayout()

        # Vertical Layout -> Remove TextEdit
        removeList = QtWidgets.QTreeWidget()

        removeList.setColumnCount(2)
        removeList.setRootIsDecorated(False)

        removeList.header().setVisible(True)
        removeList.headerItem().setText(0, "Path")
        removeList.headerItem().setText(1, "Status")

        removeList.setSelectionMode(
            QtWidgets.QAbstractItemView.SelectionMode.NoSelection
        )

        for item in self.__resultInfo:
            status = (
                "Will be deleted"
                if self.__removalType.casefold() == DELETE_OPTION.casefold()
                else "Will be hidden"
            )
            if item.type == RemovalEntryType.INVALID:
                status = "File or directory does not exist."
            elif (
                item.type == RemovalEntryType.DIRECTORY
                and self.__removalType.casefold() == HIDDEN_OPTION.casefold()
            ):
                status = "All child files in this folder will be hidden RECURSIVELY."
            treeItem = QtWidgets.QTreeWidgetItem(removeList, [str(item.path), status])
            for x in range(2):
                treeItem.setBackground(
                    x,
                    (
                        QColor(205, 222, 135)
                        if item.type != RemovalEntryType.INVALID
                        else QColor(255, 170, 170)
                    ),
                )
                treeItem.setForeground(x, qtBlack)
                treeItem.setData(
                    x, qtUserRole, {"path": str(item.path), "status": status}
                )
            removeList.addTopLevelItem(treeItem)
        removeList.resizeColumnToContents(0)

        verticalLayout.addWidget(removeList)

        buttonLayout = QtWidgets.QHBoxLayout()

        removeButton = QtWidgets.QPushButton("Remove All", self)
        removeButton.clicked.connect(self.executeRemoval)
        buttonLayout.addWidget(removeButton)

        invertButton = QtWidgets.QPushButton("Invert", self)
        invertButton.clicked.connect(self.invertFiles)
        buttonLayout.addWidget(invertButton)

        cancelButton = QtWidgets.QPushButton("Cancel", self)
        cancelButton.clicked.connect(self.closeWindow)
        buttonLayout.addWidget(cancelButton)

        verticalLayout.addLayout(buttonLayout)

        self.setLayout(verticalLayout)

    def closeWindow(self) -> None:
        self.close()

    def closeEvent(self, event):
        settings = QSettings(PluginTool.AUTHOR, PluginTool.NAME)
        settings.setValue("geometry-confirm", self.saveGeometry())
        QtWidgets.QDialog.closeEvent(self, event)

    def fileSafeModName(self) -> str:
        return "".join(
            [c for c in self.__mod.name() if c.isalpha() or c.isdigit() or c == " "]
        )

    def invertFiles(self):
        result: list[RemovalEntry] = []
        modPath = Path(self.__mod.absolutePath())
        # Ignore meta.ini
        metaPath = os.path.join(modPath, "meta.ini")
        self.__resultInfo.append(RemovalEntry(metaPath, RemovalEntryType.FILE))

        logPath = os.path.join(
            modPath, self.fileSafeModName() + "." + self.__removalType + ".log"
        )
        if os.path.exists(logPath):
            self.__resultInfo.append(RemovalEntry(logPath, RemovalEntryType.FILE))

        pd = QProgressDialog(
            "Inverting files. This WILL take a while...", "Abort", 0, 0
        )
        pd.setWindowModality(Qt.WindowModality.WindowModal)
        pd.setMinimumDuration(0)
        pd.setValue(-1)

        for root, dirs, files in os.walk(modPath):
            QApplication.processEvents()
            for dir in dirs:
                fullPath = Path(root, dir)
                if not any(Path(x.path) in fullPath.parents for x in result):
                    if not any(
                        Path(x.path) in fullPath.parents
                        or fullPath in Path(x.path).parents
                        for x in self.__resultInfo
                    ):
                        if not any(
                            x.type != RemovalEntryType.INVALID
                            and os.path.samefile(x.path, fullPath)
                            for x in self.__resultInfo
                        ):
                            result.append(
                                RemovalEntry(str(fullPath), RemovalEntryType.DIRECTORY)
                            )
            for file in files:
                if os.path.splitext(file)[-1].lower() == ".mohidden":
                    continue
                fullPath = Path(root, file)
                if not any(Path(x.path) in fullPath.parents for x in result):
                    if not any(
                        Path(x.path) in fullPath.parents for x in self.__resultInfo
                    ):
                        if not any(
                            x.type != RemovalEntryType.INVALID
                            and os.path.samefile(x.path, fullPath)
                            for x in self.__resultInfo
                        ):
                            result.append(
                                RemovalEntry(str(fullPath), RemovalEntryType.FILE)
                            )

        pd.done(0)

        warningMsg = QMessageBox()
        warningMsg.setIcon(QMessageBox.Icon.Critical)
        warningMsg.setText("Inverse deleting is EXPERIMENTAL.")
        warningMsg.setInformativeText(
            """
Carefully inspect the removal list before clicking "Remove All".
Depending on original input, and if you have file deletion enabled, this can cause irrecoverable loss of files.
"""
        )
        warningMsg.setWindowTitle("Warning")
        warningMsg.setStandardButtons(QMessageBox.StandardButton.Ok)
        warningMsg.exec()
        self.close()
        confirmationWindow = ConfirmationWindow(
            self.__mod, self, result, self.__organizer
        )
        confirmationWindow.open()

    def executeRemoval(self):
        removed: list[str] = []
        optionalFailed: list[OptionalEntry] = []
        failed: list[str] = []
        errors: list[str] = []

        pd = QProgressDialog("Removing files...", "Abort", 0, len(self.__resultInfo))
        pd.setMinimumDuration(0)
        pd.setWindowModality(Qt.WindowModality.WindowModal)
        progress = 0

        for item in self.__resultInfo:
            if pd.wasCanceled():
                break
            pd.setValue(progress)
            progress += 1
            if item.type != RemovalEntryType.INVALID:
                try:
                    if self.__removalType.casefold() == HIDDEN_OPTION.casefold():
                        hiddenResult = self.hideItem(item)
                        if isinstance(hiddenResult, WalkedHiddenResult):
                            removed.extend(hiddenResult.succeeded)
                            for failedEntry in hiddenResult.failed:
                                failed.append(failedEntry.path)
                                if self.__debugEnabled:
                                    errors.append(failedEntry.stackTrace)
                        elif isinstance(hiddenResult, OptionalEntry):
                            if hiddenResult.failed:
                                optionalFailed.append(hiddenResult)
                            else:
                                removed.append(f"{item.path} -> {hiddenResult.path}")
                        else:
                            removed.append(item.path)
                    elif self.__removalType.casefold() == DELETE_OPTION.casefold():
                        self.deleteItem(item)
                        removed.append(item.path)
                except Exception:
                    if self.__debugEnabled:
                        errors.append(traceback.format_exc())
                    failed.append(item.path)

        pd.setValue(pd.maximum())

        logFileName = self.fileSafeModName() + "." + self.__removalType + ".log"
        logPath = os.path.join(self.__mod.absolutePath(), logFileName)
        with open(logPath, "a+") as f:
            if os.path.getsize(logPath) > 0:
                f.write("\n\n")
            f.write(
                str.center(
                    "The following files were removed on "
                    + time.strftime("%a, %d %b %Y %H:%M:%S"),
                    LOG_WIDTH,
                    "-",
                )
                + "\n"
            )
            if self.__pluginOptional:
                f.write(
                    str.center(
                        "Note that optional files are formatted <path> -> <destPath> because they have been moved.",
                        LOG_WIDTH,
                        "-",
                    )
                )
                f.write("\n")
            f.write("\n".join(removed))
            if len(optionalFailed) > 0:
                f.write(
                    "\n" + str.center("Duplicate Optional Files", LOG_WIDTH, "-") + "\n"
                )
                for optional_fail in optionalFailed:
                    dest_path = os.path.join(
                        self.__mod.absolutePath(),
                        "optional",
                        os.path.basename(optional_fail.path),
                    )
                    f.write(f"{optional_fail.path} -> {dest_path}\n")
            if len(failed) > 0:
                f.write(str.center("Failed Files", LOG_WIDTH, "-") + "\n")
                f.write("\n".join(failed))
            if self.__debugEnabled and len(errors) > 0:
                f.write("\n" + str.center("Errors", 100, "-") + "\n")
                f.write("\n".join(errors))
        if len(optionalFailed) > 0:
            optionalWarningMsg = QMessageBox()
            optionalWarningMsg.setIcon(QMessageBox.Icon.Critical)
            optionalWarningMsg.setText(
                f"Some optional files failed to be moved due to duplicate plugins in the optional folder. Check {logPath} for more details."
            )
            optionalWarningMsg.setWindowTitle("Warning")
            optionalWarningMsg.setStandardButtons(QMessageBox.StandardButton.Ok)
            optionalWarningMsg.resize(500, 200)
            optionalWarningMsg.exec()
        if len(failed) > 0:
            warningMsg = QMessageBox()
            warningMsg.setIcon(QMessageBox.Icon.Critical)
            warningMsg.setText("Removal failed to process on some files.")
            warningMsg.setDetailedText("\n".join(failed))
            warningMsg.setWindowTitle("Warning")
            warningMsg.setStandardButtons(QMessageBox.StandardButton.Ok)
            warningMsg.resize(500, 200)
            warningMsg.exec()
        self.close()

    def deleteItem(self, item: RemovalEntry):
        if item.type == RemovalEntryType.FILE:
            os.remove(item.path)
        elif item.type == RemovalEntryType.DIRECTORY:
            shutil.rmtree(item.path)

    def hideItem(self, item: RemovalEntry) -> WalkedHiddenResult | OptionalEntry | None:
        if item.type == RemovalEntryType.FILE:
            # Skip the file if it doesn't exist. Explaination:
            # If we got this far and the file doesn't exist,
            # the file was matched twice. This is very hacky,
            # but for now this is a decent solution that allows
            # recursive mohidden functionality.
            if os.path.exists(item.path):
                if (
                    self.__pluginOptional
                    and os.path.splitext(item.path)[-1].lower() in PLUGIN_EXTS
                    and isdirectsubpath(self.__mod.absolutePath(), item.path)
                ):
                    optional_folder = os.path.join(
                        self.__mod.absolutePath(), "optional"
                    )
                    destination_path = os.path.join(
                        optional_folder, os.path.basename(item.path)
                    )
                    if os.path.exists(destination_path):
                        result = OptionalEntry(True, item.path)
                        return result
                    if not os.path.exists(optional_folder):
                        os.mkdir(optional_folder)
                    shutil.move(item.path, optional_folder)
                    return OptionalEntry(False, destination_path)
                elif os.path.splitext(item.path)[-1].lower() not in PLUGIN_EXTS:
                    os.rename(item.path, item.path + ".mohidden")
        elif item.type == RemovalEntryType.DIRECTORY:
            result = WalkedHiddenResult()
            try:
                for root, _, files in os.walk(item.path):
                    for file in files:
                        if os.path.splitext(file)[-1].lower() == ".mohidden":
                            continue

                        fullPath = os.path.join(root, file)
                        try:
                            os.rename(fullPath, fullPath + ".mohidden")
                            result.succeeded.append(fullPath)
                        except:
                            result.failed.append(
                                WalkFailedEntry(fullPath, traceback.format_exc())
                            )
            except:
                result.walkFailed = True
                result.failed.append(WalkFailedEntry(item.path, traceback.format_exc()))
            return result


class PluginTool(IPluginTool):
    NAME = "MO2 Removal Tool"
    AUTHOR = "DeltaJordan"
    DESCRIPTION = "Deletes or hides specified files in mod and adds a note describing which files were removed."

    def __init__(self):
        super(PluginTool, self).__init__()

    def init(self, organizer: IOrganizer):
        self.__organizer = organizer
        return True

    def settings(self) -> List[PluginSetting]:
        return [
            PluginSetting("enabled", "Enable plugin", True),
            PluginSetting(
                "removal-type",
                "In what way should files be removed? "
                + HIDDEN_OPTION
                + " (Hide the file(s)), "
                + DELETE_OPTION
                + " (Delete the file(s)). This field is NOT case sensitive.",
                "unset",
            ),
            PluginSetting(
                "plugin-optional",
                "If  using "
                + HIDDEN_OPTION
                + ", and True, this uses the MO2 optional folder to disable plugins instead of .mohidden.",
                False,
            ),
            PluginSetting(
                "debug-log", "Adds debug logging. Not intended for users.", False
            ),
        ]

    def display(self):
        self.__window = PluginWindow(self.__organizer)
        self.__window.setWindowTitle(self.NAME)
        self.__window.exec()

        self.__organizer.refresh()

    def icon(self) -> QIcon:
        return QIcon(":/MO/gui/remove")

    def setParentWidget(self, parent: QWidget):
        self.__parentWidget = parent

    def version(self: IPlugin) -> VersionInfo:
        # Increment Guide: {Major|Breaking}.{Feature}.{Minor|Bugfix}
        return VersionInfo(2, 4, 0, ReleaseType.FINAL)

    def description(self) -> str:
        return self.DESCRIPTION

    def tooltip(self) -> str:
        return self.DESCRIPTION

    def displayName(self) -> str:
        return self.NAME

    def name(self) -> str:
        return self.NAME

    def author(self) -> str:
        return PluginTool.AUTHOR


def createPlugin() -> IPlugin:
    return PluginTool()
